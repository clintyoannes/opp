
<!DOCTYPE html>
<html lang="en">

<html>
<head>
  <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Release 0 Index</title>
</head>

<body>
<?php
require_once "animal.php";
require_once "frog.php";
require_once "ape.php";

$sheep = new Animal("shaun");
echo $sheep->name . '<br>'; // "shaun"
echo $sheep->legs. '<br>'; // 2
echo $sheep->cold_blooded.'<br>'.'<br>'; // false

$sungokong = new ape("kera sakti");
$sungokong->yell();// "Auooo"

$kodok = new frog("buduk");
$kodok -> jump() ; // "hop hop"
?>
</body>
</html>