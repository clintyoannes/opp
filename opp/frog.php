<!DOCTYPE html>
<html lang="en">

<html>
<head>
  <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Release 1 Animal Frog</title>
</head>

<body>
<?php
require_once "animal.php";
class frog extends animal{
    public $cold_blooded = "true";
    public $legs = 4;
    public function jump(){
        echo 'hop hop'."<br>";
    }
}
?>
</body>
</html>